#!/usr/bin/env python2
from __future__ import print_function, absolute_import, division

import logging

from collections import defaultdict
from errno import ENOENT, EACCES
from stat import S_IFDIR, S_IFLNK, S_IFREG
from sys import argv, exit
from time import time

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn
import os.path

if not hasattr(__builtins__, 'bytes'):
    bytes = str

class DuFS(LoggingMixIn, Operations):

    def __init__(self, dudata):
        self.fd = 0
        self.now = time()
        self.bsz = 1024
        self.files = {}

        count = 0
        for r in dudata:
            try:
                r = r.decode('utf-8')
            except:
                r = r.decode('latin-1')

            sz, path = tuple(r.split("\t", 1))
            sz = int(sz)
            path = path.strip()
            o = self.walkpath(path, True)
            o[".size"]=sz
            count += 1
        self.count = count
        self.total = o[".size"]
        def fixsz(o):
            sz = o.get(".size", 0)
            for k,v in o.iteritems():
                if k!=".size":
                    sz-=v.get(".size", 0)
                    fixsz(v)
            assert(sz>=0)
            o[".size"]=sz
        fixsz(self.files)

    def walkpath(self, path, create=False):
        path = os.path.normpath(path)
        out = self.files
        for p in path.split("/"):
            if len(p)==0 or p==".":
                continue
            if p not in out:
                if create:
                    out[p]={}
                else:
                    raise FuseOSError(ENOENT)
            out = out[p]
        return out

    def getattr(self, path, fh=None):
        o = self.walkpath(path)
        ret = dict(st_mode=0, st_ctime=self.now,
                st_mtime=self.now, st_atime=self.now, st_nlink=1,
                st_dev = 0, st_uid = 0, st_gid = 0,
                st_size = 0, st_blocks = 0, st_blksize = self.bsz)
        if isinstance(o, dict):
            ret["st_mode"] |= S_IFDIR | 0111
        else:
            ret["st_mode"] |= S_IFREG
            ret["st_size"] = o*self.bsz
            ret["st_blocks"] = o
            ret["st_blksize"] = self.bsz
        return ret

    def open(self, path, flags):
        raise FuseOSError(EACCES)

    def read(self, path, size, offset, fh):
        raise FuseOSError(EACCES)

    def readdir(self, path, fh):
        o = self.walkpath(path)
        if not isinstance(o, dict):
            raise FuseOSError(ENOTSUP)
        return ['.', '..'] + o.keys()

    def statfs(self, path):
        return dict(
                f_bsize = self.bsz,
                f_bfree = 0,
                f_bavail = 0,
                f_files = self.count,
                f_free = 0,
                f_fsid = 0)

if __name__ == '__main__':
    if len(argv) != 3:
        print('usage: %s <du output> <mountpoint>' % argv[0])
        exit(1)

    #logging.basicConfig(level=logging.DEBUG)
    with open(argv[1], "r") as fd:
        fuse = FUSE(DuFS(fd), argv[2], foreground=True)
