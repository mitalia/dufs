# DuFS - a virtual filesystem for mounting Linux `du` output #

`DuFS` is a Python FUSE filesystem that creates a virtual directories tree mimicking the structure and disk usage of a previously saved `du` output.

The idea behind it is that we have really nice graphical tools to explore the major offenders in disk space usage (personally, I really like [Filelight](https://en.wikipedia.org/wiki/Filelight)), but often you have to investigate this kind of problems on servers without any graphical environment, Android phones, NAS and other more-or-less embedded devices.

In these cases, good old `du` is generally available (typically in the `busybox` implementation), but parsing effectively its output quickly becomes a daunting task.

Enters `DuFS`. The typical workflow is:

- you run `du > du_output.txt` on the device;
- move `du_output.txt` on your nice graphical workstation;
- call `dufs.py du_output.txt mountpoint`
- run `filelight`  or whatever GUI tool you like over `mountpoint`

Now you'll be able to explore the `du` output through your GUI tool, as if you mounted the filesystem of the device over `mountpoint`.

Currently, the virtual filesystem is readonly. In future I may let users delete files, with `dufs.py` generating the corresponding shell script to be run on the target device at the end.

### Implementation notes ###

- `dufs.py` uses [`fusepy`](https://github.com/fusepy/fusepy); you can easily install it with `pip install fusepy`.
- the script interprets every row in the file as a directory, although you *can* give it files generated with `du -a` and you'll still get meaningful results;
- every directory in the mounted file system contains a `.size` file as big as the sum of all the files of the directory in the original filesystem;
- although POSIX recommends `du` to output sizes in number of 512-bytes blocks, everybody (and especially `busybox`) actually outputs way more reasonable 1024-bytes blocks counts; `dufs.py` does expect 1024 bytes blocks counts. If you have a file with different block size, you can either ignore the problem (the proportions between sizes of the directory elements will still be correct), or change the value of the `bsz` of the `DuFS` class.